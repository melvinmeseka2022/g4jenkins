resource "aws_vpc" "g4vpc" {
  cidr_block       = "10.0.0.0/16"
  enable_dns_hostnames = "true"
#   instance_tenancy = "default"

  tags = {
    Name = "g4vpc"
  }
}


resource "aws_internet_gateway" "g4igw" {
  vpc_id = aws_vpc.g4vpc.id

  tags = {
    Name = "g4igw"
  }
}



resource "aws_subnet" "plksubnet1" {
  vpc_id     = aws_vpc.g4vpc.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "plksubnet1"
  }
}

resource "aws_subnet" "plksubnet2" {
  vpc_id     = aws_vpc.g4vpc.id
  cidr_block = "10.0.2.0/24"

  tags = {
    Name = "plksubnet2"
  }
}


resource "aws_route_table" "g4rt_table" {
  vpc_id = aws_vpc.g4vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.g4igw.id
  }

#   route {
#     ipv6_cidr_block        = "::/0"
#     egress_only_gateway_id = aws_egress_only_internet_gateway.example.id
#   }

  tags = {
    Name = "g4rt_table"
  }
}

resource "aws_route_table_association" "g4rtassociation1" {
  subnet_id      = aws_subnet.plksubnet1.id
  route_table_id = aws_route_table.g4rt_table.id
}


resource "aws_route_table_association" "g4rtassociation2" {
  subnet_id      = aws_subnet.plksubnet2.id
  route_table_id = aws_route_table.g4rt_table.id
}


resource "aws_security_group" "g4sg" {
  name        = "g4sg"
  description = "Allow HTTP SSH Jenkins"
  vpc_id      = aws_vpc.g4vpc.id

  ingress {
    description      = "TLS from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    # ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
  }

  ingress {
    description      = "Jenkins"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    # ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
  }

  ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    # ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}


# resource "aws_network_interface" "g4ENI1" {
#   subnet_id       = aws_subnet.plksubnet1.id
#   private_ips     = ["10.0.1.50"]
#   security_groups = [aws_security_group.g4sg.id]

# #   attachment {
# #     instance     = aws_instance.test.id
# #     device_index = 1
# #   }
# }

# resource "aws_network_interface" "g4ENI2" {
#   subnet_id       = aws_subnet.plksubnet2.id
#   private_ips     = ["10.0.2.50"]
#   security_groups = [aws_security_group.g4sg.id]

#   attachment {
#     instance     = aws_instance.test.id
#     device_index = 1
#   }
# }


# resource "aws_eip" "g4EIP" {
#   instance = aws_instance.web.id
#   vpc      = true
# }

# resource "aws_eip" "g4EIP" {
#   instance = aws_instance.web.id
#   vpc      = true
# }


